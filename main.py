from vars import *
import random

#choice  = random.choice(list)


def menu():
    order = input('Vad vill du göra?\n1: Slumpa fram ett ämne\n2: Redigera kompetenser\n')
    if order == '1':
        main_func()
    if order == '2':
        pass


def main_func():
    choice  = random.choice(list)
    f = open('file.txt', 'a')
    global kub
    global ter
    global aws
    global pyt
    global git
    global ci
    global jen
    global ba
    global sql
    global lin

    if choice == 'SQL':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            sql = sql + 1
            f.write(choice + ' har valts ' + str(sql) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'Python':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            pyt = pyt + 1
            f.write(choice + ' har valts ' + str(pyt) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'Git':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            git = git + 1
            f.write(choice + ' har valts ' + str(git) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'AWS/Azure':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            aws = aws +1
            f.write(choice + ' har valts ' + str(aws) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'Linux/Ubuntu':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            lin = lin +1
            f.write(choice + ' har valts ' + str(lin) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'CI/CD':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            ci = ci+1
            f.write(choice + ' har valts ' + str(ci) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'Kubernetes/Docker':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            kub= kub +1
            f.write(choice + ' har valts ' + str(kub) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'Terraform':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            ter = ter +1
            f.write(choice + ' har valts ' + str(ter) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'Jenkins':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            jen = jen +1
            f.write(choice + ' har valts ' + str(jen) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()
    if choice == 'Bash':
        subject = input('Dagens ämne blir: ' + choice + '\nVill du:\n1: Behålla\n2: Byta?\n')
        if subject == '1':
            ba = ba +1
            f.write(choice + ' har valts ' + str(ba) + ' gånger\n')
            print('Lycka till!')
            input()
        if subject == '2':
            menu()

def view_file():
    v = open('file.txt', 'r')
    print(v.read())

if __name__ == '__main__':
    menu()
